const expressJwt = require('express-jwt');
const config = require('./config.json');
const  { getUserByID }  = require('../users/users.controller')

module.exports = jwt;

function jwt(roles = []) {
    if(typeof roles === 'string') {
        roles = [roles]
    }
    const secret = config.secret;
    return [
        expressJwt({ secret, isRevoked }),

        (req, res, next) => {
            if (roles.length && !roles.includes(req.user.role)) {
                return res.status(403).json({ message: 'Unauthorized' });
            }
            next();
        }
    ]
    
}

async function isRevoked(req, payload, done) {
    const user = await getUserByID(payload.sub)

    if (!user) {
        return done(null, true);
    }

    done();
};