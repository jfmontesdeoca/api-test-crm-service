const router = require('express').Router()
const { authenticate } = require('./../users/users.controller')

router.post('/', authentication)

module.exports.blueprint = `
# Group AUTH
## Auth [/api/auth]
For making requests to **/api/users** and **/api/customers**, you have to authenticate.
You can authenticate sending \`username\` and \`password\`, and if the authentication is successful, the API will send an authorization token in the response
### Post Auth [POST]
+ Request with body (application/json)
    + Body
    {
        "username": "user",
        "password": "pass"
    }
+ Response 200 (application/json)
  + Body
  {
    "token": "authToken"
}
+ Response 401 (application/json)
  + Body
  {
    "message": "Username or password is incorrect"
}
+ Request without body or without 'user' or 'password' in the body (application/json)
+ Response 400 (application/json)
  + Body
  {
    "message": "You have to provide an username and a password"
  }
`
function authentication(req, res, next) {
    authenticate(req.body)
        .then(user => user ? res.json({token: user.token}) : res.status(401).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err))
}

module.exports.router = router