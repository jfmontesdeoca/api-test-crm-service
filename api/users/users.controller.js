const USER = require('./users.model')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const config = require('./../auth/config.json')

async function getAllUsers() {
    return await USER.find().select('-password -__v')
}

async function getUserByID(id) {
    return await USER.findById(id).select('-password -__v')
}

async function deleteUserByID(id) {
    return await USER.findByIdAndDelete(id).select('-password -__v')
}

async function modifyUserByID(id, userParam) {
    const user = await USER.findById(id)
    if (!user) throw 'User not found'
    if (user.username !== userParam.username && await USER.findOne({ username: userParam.username })) {
        throw `Username '${userParam.username}' is already taken`
    }
    if (userParam.password) userParam.password = bcrypt.hashSync(userParam.password)
    Object.assign(user, userParam)
    return await user.save()
}

async function createUser(user) {
    if (user.username && user.password) {
        if (await USER.findOne({ username: user.username })) {
            throw `Username '${user.username}' is already taken`
        }
        const newUser = new USER(user)
        newUser.password = bcrypt.hashSync(user.password)
        return await newUser.save()
    } else {
        throw "You have to provide an username and a password"
    }
}

async function authenticate({ username, password }) {
    if (username && password) {
        const user = await USER.findOne({ username })
        if (user && bcrypt.compareSync(password, user.password)) {
            const { password, ...userWithoutPassword } = user.toObject()
            const token = jwt.sign({ sub: user.id, role: user.role }, config.secret)
            return {
                ...userWithoutPassword,
                token
            }
        }
    } else {
        throw "You have to provide an username and a password"
    }
}

module.exports = {
    getAllUsers, getUserByID, deleteUserByID, modifyUserByID, createUser, authenticate
}