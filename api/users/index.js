const router = require('express').Router()
const usersController = require('./users.controller')

router.get('/', getAllUsers)
router.get('/:user', getUserByID)
router.delete('/:user', deleteUserByID)
router.patch('/:user', modifyUserByID)
router.post('/', createUser)

function getAllUsers(req, res, next) {
    usersController.getAllUsers()
        .then(users => res.json(users))
        .catch(err => next(err))
}

function getUserByID(req, res, next) {
    usersController.getUserByID(req.params.user)
        .then(user => user ? res.json(user) : res.status(404).json({"message": 'User not found'}))
        .catch(err => next(err))
}

function deleteUserByID(req, res, next) {
    usersController.deleteUserByID(req.params.user)
        .then(user => user ? res.json(user) : res.status(404).json({"message": 'User not found'}))
        .catch(err => next(err))
}

function modifyUserByID(req, res, next) {
    usersController.modifyUserByID(req.params.user, req.body)
        .then(user => user ? res.json(filterUser(user)) : res.status(404).json({"message": 'User not found'}))
        .catch(err => next(err))
}

function createUser(req, res, next) {
    usersController.createUser(req.body)
        .then(user => user ? res.status(201).json(filterUser(user)) : res.sendStatus(400))
        .catch(err => next(err))
}

function filterUser(user) {
    const { password, __v, ...userFiltered } = user.toObject()
    return userFiltered
}

module.exports.blueprint = `
# Data Structures

## UserData
+ role: User - Role of the user, can be User or Admin
+ _id: 5d34d5f6c9e86700225c05a5 - Unique identifier
+ username: johndoe - Username (unique)
## UserList (array)
+ (UserData)

## NoToken
+ message: Invalid Token

## Unauthorized
+ message: Unauthorized

# Group USERS
## Users [/api/users]


### Get Users [GET]
Get a list of users.
+ Request with token and being admin (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200 (application/json)
    + Attributes (UserList)
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Request with token but being user (application/json)
    + Headers
        Authorization: Bearer token
+ Response 403 (application/json)
    + Attributes (Unauthorized)

### Create New User [POST]
Create a new user using username, password and role. ID is autoassigned. Role param is optional, its default value is \`User\`.

+ Request with body (application/json)
    + Headers
        Authorization: Bearer token
    + Body 
        {
            "username": "johndoe",
            "password": "pass",
            "role": "Admin"
        }
+ Response 201
    + Body 
        {
            "role": "Admin",
            "_id": "5d3874272c816911660ec445",
            "username": "johndoe"
        }
+ Response 400
    + Body
        {
            "message": "Username 'johndoe' is already taken"
        }
+ Request without body or without 'user' and 'password (application/json)
    + Headers
        Authorization: Bearer token
+ Response 400
    + Body 
    {
        "message": "You have to provide an username and a password"
    }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Request with token but being user (application/json)
    + Headers
        Authorization: Bearer token
+ Response 403 (application/json)
    + Attributes (Unauthorized)

## User [/api/users/{id}]
+ Parameters
    + id: \`5d34d5f6c9e86700225c05a5\` (required, string) - The user ID
### Get User [GET]
Get a single user
+ Request (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200 (application/json)
    + Attributes (UserData)
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Request with token but being user (application/json)
    + Headers
        Authorization: Bearer token
+ Response 403 (application/json)
    + Attributes (Unauthorized)

### Patch User [PATCH]
Modify a single user information. You can use this for change admin status on a user.
+ Request (application/json)
    + Headers
        Authorization: Bearer token
    + Attributes (UserData)
+ Response 200 (application/json)
    + Body
        {
            "username": "johndoe",
            "password": "pass",
            "role": "Admin"
        }
+ Response 400
    + Body
            {
                "message": "Username 'johndoe' is already taken"
            }
+ Response 404
    + Body
            {
                "message": "User not found"
            }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Request with token but being user (application/json)
    + Headers
        Authorization: Bearer token
+ Response 403 (application/json)
    + Attributes (Unauthorized)


### Delete a User [DELETE]
Delete a single user
+ Request (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200
    + Body
        {
            "username": "johndoe",
            "password": "pass",
            "role": "Admin"
        }
+ Response 404 (application/json)
    + Body
        {
            "message": "User not found"
        }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Request with token but being user (application/json)
    + Headers
        Authorization: Bearer token
+ Response 403 (application/json)
    + Attributes (Unauthorized)

`
module.exports.router = router