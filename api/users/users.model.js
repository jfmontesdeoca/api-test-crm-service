const mongoose = require ('mongoose')

const USERschema = mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: "User"
    }
})

const USER = mongoose.model('user', USERschema)
module.exports = USER