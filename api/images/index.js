const router = require('express').Router()

const multer = require("multer")
const cloudinary = require("cloudinary")
const cloudinaryStorage = require("multer-storage-cloudinary")

const config = require('./.env')
const options = config["config"]

const _CLOUD_NAME = options.CLOUD_NAME
const _API_KEY = options.API_KEY
const _API_SECRET = options.API_SECRET

cloudinary.config({
    cloud_name: _CLOUD_NAME,
    api_key: _API_KEY,
    api_secret: _API_SECRET
})

const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: "uploads",
    allowedFormats: ["jpg", "png"],
    transformation: [{ width: 500, height: 500, crop: "limit" }]
})

const parser = multer({ storage: storage })

module.exports.blueprint = `
# Group IMAGES
## Images [/api/images]


### Post Images [POST]
Upload a new image. Need to be authenticated to upload a image, but every user can do this. Returns the URL wheres the image was uploaded.

+ Request with token (multipart/form-data)
    + Headers
        Authorization: Bearer token
+ Response 200 (application/json)
    + Body
        {
            "url": "http://res.cloudinary.com/dsedushbs/image/upload/v1563986154/uploads/nwnbqnncl5zezoec9yg9.png",
            "secure_url": "https://res.cloudinary.com/dsedushbs/image/upload/v1563986154/uploads/nwnbqnncl5zezoec9yg9.png"
        }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)

`
router.post('/', parser.single('image'), (req, res) => {
    if (req.file) {
        const {url, secure_url} = req.file
        res.json({url, secure_url})
    } else {
        throw "Not image was sent"
    }
})

module.exports.router = router