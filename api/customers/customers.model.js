const mongoose = require ('mongoose')

const CUSTOMERschema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    photoURL: String,
    creatorId: {
        type: String,
        required: true
    },
    lastModifiedBy: String
})

const CUSTOMER = mongoose.model('customer', CUSTOMERschema)
module.exports = CUSTOMER