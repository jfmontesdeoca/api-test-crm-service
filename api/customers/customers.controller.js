const CUSTOMER = require('./customers.model')

async function getAllCustomers() {
    return await CUSTOMER.find().select('-__v')
}

async function getCustomerByID(id) {
    return await CUSTOMER.findById(id).select('-__v')
}

async function deleteCustomerByID(id) {
    return await CUSTOMER.findByIdAndDelete(id).select('-__v')
}

async function modifyCustomerByID(id, customerParam, user) {
    const customer = await CUSTOMER.findById(id)
    if (!customer) throw `Customer not found`
    customerParam.lastModifiedBy = user.sub
    Object.assign(customer, customerParam)
    return await customer.save()
}

async function createCustomer(customer, user) {
    if (customer.name && customer.surname) {
        const newCustomer = new CUSTOMER(customer)
        newCustomer.creatorId = user.sub
        newCustomer.lastModifiedBy = user.sub
        return await newCustomer.save()
    } else {
        throw "You have to provide a name and a surname"
    }
}

module.exports = {
    getAllCustomers, getCustomerByID, deleteCustomerByID, modifyCustomerByID, createCustomer
}