const router = require('express').Router()
const customersController = require('./customers.controller')

router.get('/', getAllCustomers)
router.get('/:customer', getCustomerByID)
router.delete('/:customer', deleteCustomerByID)
router.patch('/:customer', modifyCustomerByID)
router.post('/', createCustomer)

function getAllCustomers(req, res, next) {
    customersController.getAllCustomers()
    .then(customers => res.json(customers))
    .catch(err => next(err))
}

function getCustomerByID(req, res, next) {
    customersController.getCustomerByID(req.params.customer)
    .then(customer => customer ? res.json(customer) : res.status(404).json({"message": 'Customer not found'}))
    .catch(err => next(err))
}

function deleteCustomerByID(req, res, next) {
    customersController.deleteCustomerByID(req.params.customer)
    .then(customer => customer ? res.json(customer) : res.status(404).json({"message": 'Customer not found'}))
    .catch(err => next(err))
}

function modifyCustomerByID(req, res, next) {
    customersController.modifyCustomerByID(req.params.customer, req.body, req.user)
    .then(customer => customer ? res.json(filterCustomer(customer)) : res.status(404).json({"message": 'Customer not found'}))
    .catch(err => next(err))
}

function createCustomer(req, res, next) {
    customersController.createCustomer(req.body, req.user)
    .then(customer => customer ? res.status(201).json(filterCustomer(customer)) : res.sendStatus(400))
    .catch(err => next(err))
}

function filterCustomer(customer) {
    const {  __v, ...customerFiltered } = customer.toObject()
    return customerFiltered
}

module.exports.blueprint = `
# Data Structures

## CustomerData
+ name: John - Customer's name
+ surname: Doe - Customer's surname
+ photoURL: https://res.cloudinary.com/dsedushbs/image/upload/v1563986266/uploads/rpheqb4ohc8njanvyuhv.png - URL linking to customer's photo
+ creatorID: 5d34b9ceb16fdd15f3fa00ab - A reference to the user who create this customer
+ lastModifiedBy: 5d34b9ceb16fdd15f3fa00ab - A reference to the last user who modified this customer

## CustomerList (array)
+ (CustomerData)

# Group CUSTOMERS
## Customers [/api/customers]

### Get Customers [GET]
Get a list of customers.
+ Request with token (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200 (application/json)
    + Attributes (CustomerList)
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)

### Create New Customer [POST]
Create a new customer using name, surname and photoURL. ID is autoassigned. photoURL param is optional.

+ Request with body (application/json)
    + Headers
        Authorization: Bearer token
    + Body 
        {
            "name": "John",
            "surname": "Doe",
            "photoURL": "https://res.cloudinary.com/dsedushbs/image/upload/v1563986266/uploads/rpheqb4ohc8njanvyuhv.png"
        }
+ Response 201
    + Attributes (CustomerData)
+ Request without body or without 'user' and 'password (application/json)
    + Headers
        Authorization: Bearer token
+ Response 400
    + Body 
    {
        "message": "You have to provide a name and a surname"
    }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)

## Customer [/api/customer/{id}]
+ Parameters
    + id: \`5d34d5f6c9e86700225c05a5\` (required, string) - The customer ID
### Get Customer [GET]
Get a single customer
+ Request (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200 (application/json)
    + Attributes (CustomerData)
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
+ Response 404
    + Body
        {
            "message": "Customer not found"
        }

### Patch Customer [PATCH]
Modify a single customer information. 
+ Request (application/json)
    + Headers
        Authorization: Bearer token
    + Body
        {
            "name": "John",
            "surname": "Doe",
            "photoURL": "https://res.cloudinary.com/dsedushbs/image/upload/v1563986266/uploads/rpheqb4ohc8njanvyuhv.png"
        }
+ Response 200 (application/json)
    + Attributes (CustomerData)
+ Response 404
    + Body
        {
            "message": "Customer not found"
        }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)

### Delete a User [DELETE]
Delete a single user
+ Request (application/json)
    + Headers
        Authorization: Bearer token
+ Response 200
    + Attributes (CustomerData)
+ Response 404 (application/json)
    + Body
        {
            "message": "Customer not found"
        }
+ Request without token(application/json)
+ Response 401 (application/json)
    + Attributes (NoToken)
`

module.exports.router = router