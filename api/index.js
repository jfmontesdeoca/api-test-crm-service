const express = require('express')

const images = require('./images')
const customers = require('./customers')
const users = require('./users')
const auth = require('./auth')
const jwt = require('./auth/jwt')
const Role = require('./auth/role')
const errorHandler = require('./error-handler')

const { generateDocs, composeBlueprint, init } = require('./blueprint')

const app = express()
app.use(express.json())
app.use("/auth", auth.router)
app.use("/images", jwt(), images.router)
app.use("/customers", jwt(), customers.router)
app.use("/users", jwt(Role.Admin), users.router)
app.get('/docs', generateDocs([init,auth.blueprint,users.blueprint, images.blueprint, customers.blueprint]))
app.use(errorHandler)

module.exports = app