const { memoize } = require('lodash')
const { promisify } = require('util')
const aglio = require('aglio')

module.exports.generateDocs = (blueprints = []) => {
  const blueprint = blueprints.join('\n')
  const render = memoize(promisify(aglio.render))

  return (req, res) => {
    return render(blueprint, {})
      .then((html) => res.send(html))
      .catch(() => res.sendStatus(500))
  }
}

module.exports.init = `
# API Documentation: The CRM Service API
Welcome to the **CRM Service** API. This API provides access to the **customers**
and **users** in the service. Here you can find the documentation of this API.
Only registered users can use this API, so you need to authenticate.
`