# API Test - CRM Service :rocket:

***CRM Service*** is an *Express*-based app with *Node.js*

This app is a REST API to manage customer data for a small shop. It works as the backend side for a CRM interface.

## Getting started

The project is configured and running in production mode in this server: ec2-34-253-193-121.eu-west-1.compute.amazonaws.com:4000

If you want to configure yours, you should do the following steps:

- Clone the repository of the project:
`git clone https://bitbucket.org/jfmontesdeoca/api-test-crm-service.git`
- Configure your cloudinary account modifying `/api/images/.env`, there's an example configuration file in the same folder named `.env_example`.
- Now we have different options:
  - If you have `Docker` and `Docker Compose` installed, you just have to do the following steps:
    - Execute `docker-compose up` and you would have the app working after a while
  - If you don't want to use `Docker Compose`, you should provide a MongoDB to the app. You can launch one instance of the DB with this:
    `docker pull mongo`
    `docker run -p 27017:27017 -v /YOUR/ABSOLUTE/PATH/TO/THE/PROJECT/api-test-crm-service/db:/data/db mongo`
    Once you have MongoDB running, connect to it with a client and add an admin user, and then just do `npm i` for installing dependencies and `npm run prod` for starting the app

- If you want to use development environment instead of production, you should install `nodemon` and change some things in `Dockerfile` and `docker-compose.yml`, such as the ports used and the command used in `Dockerfile`.

## Documentation

In the **Getting started** section is explained how to start the project.

The API is self-documented, you can find the documentation accessing to the endpoint `/api/docs`, so you can find all the requests you can do (GET, POST, PATCH and DELETE) and the endpoints that you can reach, explaining in every case what should be the request and what should be the response. This was generated with `aglio`.

### Authentication

As **authentication** it's really important for using this API, here is explained how to authenticate.

You can authenticate sendind a `POST` request to `/api/auth`, sending a JSON object in the body like the following one:

```json
{
  "username": "user",
  "password": "pass"
}
```

If `username` and `password` are correct, then the API will respond with another JSON object like this:

```json
{
  "token": "authToken"
}
```

This is the token that you should send in your request to the rest of the endpoints of the app.

## Code Overview

### Application Structure

- `app.js`  - The entry point to our application. This file defines our express server and connects it to MongoDB using mongoose. It requires different environment options, a router for `/api` endpoint, it use the general middlewares used in the project (as `cors`, `helmet`, or `morgan`) and starts the server.
- `/api` - The folder that contains all the things that are really related to the different endpoints of the API: `/api/auth`, `/api/users`, `/api/customers` and `/api/images`. It also contains and `error-handler.js` file in which we define a middleware for handling some errors and a `blueprint.js` file in which we configure the `aglio` middleware for generating the documentation of the API. So, every endpoint mentioned before, has a folder inside this folder with the same name.
  - `/api/auth` - Defines the logic of authentication function, which is made making a POST request to this endpoint. It  includes a file for helping in authorization (`role.js`), a file for using the `jwt` library (`jwt.js`), a config file for this jwt-library (`config.json`) and the entry for this endpoint which acts as a router (`index.js`).
  - `/api/customers` -  This folder contains the router for customers endpoints (`index.js`), the mongoose model for saving customers in MongoDB (`customers.model.js`) and the controller for performing actions on customers collection of the DB (`customers.controller.js`).
  - `/api/images` - This folder contains the router for images endpoint (`index.js`) and a `.env` file with some necessary options for configurin the cloudinary service.
  - `/api/users` -  This folder contains the router for users endpoints (`index.js`), the mongoose model for saving users in MongoDB (`customers.model.js`) and the controller for performing actions on users collection of the DB (`customers.controller.js`).

> **WARNING :warning:** The `.env` of `/api/images` wasn't uploaded for security reasons (as it includes API KEYs of personal accounts). Instead, a `.env_example` file was uploaded, so if you want to use this project, you have to modify it to include your own API KEYs.  

### Dependencies

- [aglio](https://github.com/danielgtaylor/aglio) - An API Blueprint renderer with theme support that outputs static HTML.
- [bcrypt](https://github.com/dcodeIO/bcrypt.js/) - A library to help you hash passwords.
- [cloudinary](https://github.com/cloudinary/cloudinary_npm) - The NPM integration of Cloudinary, a cloud service that offers a solution to a web application's entire image management pipeline.
- [cors](https://github.com/expressjs/cors) - A node.js package for providing a [Connect](http://www.senchalabs.org/connect/)/[Express](http://expressjs.com/) middleware that can be used to enable [CORS](http://en.wikipedia.org/wiki/Cross-origin_resource_sharing) with various options.
- [expressjs](https://github.com/expressjs/express) - The server for handling and routing HTTP requests.
- [express-jwt](https://github.com/auth0/express-jwt) - Middleware for validating JWTs for authentication.
- [helmet](https://github.com/helmetjs/helmet) - A library that helps you secure your Express apps by setting various HTTP headers.
- [jsonwebtoken](https://github.com/auth0/node-jsonwebtoken) - For generating JWTs used by authentication.
- [mongoose](https://github.com/Automattic/mongoose) - For modeling and mapping MongoDB data to javascript.
- [morgan](https://github.com/expressjs/morgan) - An HTTP request logger middleware for node.js
- [multer](https://github.com/expressjs/multer) - A node.js middleware for handling `multipart/form-data`
