const express = require ('express')
const mongoose = require ('mongoose')
const cors = require('cors')
const helmet = require('helmet')
const morgan = require ('morgan')

const config = require('./.env')
const options = config[process.env.NODE_ENV]

const _PORT = options.PORT
const _DB_URL = options.DB_URL

const api = require('./api')

mongoose.connect(_DB_URL, { useNewUrlParser: true });

const app = express()

app.use(helmet())
app.use(morgan('combined'))
app.use(cors())

app.use('/api', api)

app.listen(_PORT, () => {
    console.log(`App running and listening in port ${_PORT}`)
})